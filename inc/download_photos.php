<?php 
function thb_image_export() {
	$download = isset($_GET['thb_download_photos']) ? $_GET['thb_download_photos'] : false;
	$ids = isset($_GET['ids']) ? $_GET['ids'] : false;
	
	if ($download == true && $ids) {
		$files = explode('-', $ids);
    
		$zipname = uniqid("thb-").'.zip';
		$zip = new ZipArchive();
		if ($zip->open($zipname, ZIPARCHIVE::CREATE) === TRUE) {
  		foreach ($files as $file) {
  		  $filename = get_attached_file($file, true);
  		  $zip->addFile($filename, basename($filename));
  		}
  		$zip->close();
		}
		
		header( 'Content-Type: application/zip');
		header( 'Content-disposition: attachment; filename='.$zipname);
		header( 'Content-Length: ' . filesize($zipname));
		header( 'Content-Description: File Transfer' );
		echo readfile("$zipname");
	}
}
add_action( 'init', 'thb_image_export' );